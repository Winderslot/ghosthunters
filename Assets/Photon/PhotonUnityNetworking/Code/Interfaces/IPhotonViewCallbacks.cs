﻿namespace Photon.Pun
{
#pragma warning disable CS0234 // Тип или имя пространства имен "Realtime" не существует в пространстве имен "Photon" (возможно, отсутствует ссылка на сборку).
    using Photon.Realtime;
#pragma warning restore CS0234 // Тип или имя пространства имен "Realtime" не существует в пространстве имен "Photon" (возможно, отсутствует ссылка на сборку).

    /// <summary>
    /// Empty Base class for all PhotonView callbacks.
    /// </summary>
    public interface IPhotonViewCallback
    {

    }

    /// <summary>
    /// This interface defines a callback which fires prior to the PhotonNetwork destroying the PhotonView and Gameobject.
    /// </summary>
    public interface IOnPhotonViewPreNetDestroy : IPhotonViewCallback
    {
        /// <summary>
        /// This method is called before Destroy() is initiated for a networked object. 
        /// </summary>
        /// <param name="rootView"></param>
        void OnPreNetDestroy(PhotonView rootView);
    }

    /// <summary>
    /// This interface defines a callback for changes to the PhotonView's owner.
    /// </summary>
    public interface IOnPhotonViewOwnerChange : IPhotonViewCallback
    {
        /// <summary>
        /// This method will be called when the PhotonView's owner changes.
        /// </summary>
        /// <param name="newOwner"></param>
        /// <param name="previousOwner"></param>
#pragma warning disable CS0246 // Не удалось найти тип или имя пространства имен "Player" (возможно, отсутствует директива using или ссылка на сборку).
#pragma warning disable CS0246 // Не удалось найти тип или имя пространства имен "Player" (возможно, отсутствует директива using или ссылка на сборку).
        void OnOwnerChange(Player newOwner, Player previousOwner);
#pragma warning restore CS0246 // Не удалось найти тип или имя пространства имен "Player" (возможно, отсутствует директива using или ссылка на сборку).
#pragma warning restore CS0246 // Не удалось найти тип или имя пространства имен "Player" (возможно, отсутствует директива using или ссылка на сборку).
    }

    /// <summary>
    /// This interface defines a callback for changes to the PhotonView's controller.
    /// </summary>
    public interface IOnPhotonViewControllerChange : IPhotonViewCallback
    {
        /// <summary>
        /// This method will be called when the PhotonView's controller changes.
        /// </summary>
        /// <param name="newOwner"></param>
        /// <param name="previousOwner"></param>
#pragma warning disable CS0246 // Не удалось найти тип или имя пространства имен "Player" (возможно, отсутствует директива using или ссылка на сборку).
#pragma warning disable CS0246 // Не удалось найти тип или имя пространства имен "Player" (возможно, отсутствует директива using или ссылка на сборку).
        void OnControllerChange(Player newController, Player previousController);
#pragma warning restore CS0246 // Не удалось найти тип или имя пространства имен "Player" (возможно, отсутствует директива using или ссылка на сборку).
#pragma warning restore CS0246 // Не удалось найти тип или имя пространства имен "Player" (возможно, отсутствует директива using или ссылка на сборку).
    }
}
