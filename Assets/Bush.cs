﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Bush : MonoBehaviour
{
    public GameObject Obj;
    void Start()
    {
        
    }
    void Update()
    {
        
    }
    public void OnTriggerEnter(Collider other)
    {
        Obj = other.gameObject;
        if (Obj.GetComponentInChildren<MeshRenderer>())
            Obj.GetComponentInChildren<MeshRenderer>().enabled = false;
    }
    public void OnTriggerExit(Collider other)
    {
        Obj.GetComponentInChildren<MeshRenderer>().enabled = true;
    }
}
