﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Photon.Realtime;


public class RoomManager : MonoBehaviourPunCallbacks
{
    public PlayerData localPlayer = new PlayerData();
    //public List<Photon.Realtime.Player> players = PhotonNetwork.PlayerList;
    public Text playersList;
    public void Leave()
    {
        PhotonNetwork.LeaveRoom();
    }
    public override void OnLeftRoom()
    {
        SceneManager.LoadScene(0);
    }
    public void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log(PhotonNetwork.NickName + " entered the room");
    }
    public void OnPlayerLeftRoom(Player otherPlayer)
    {
        Debug.Log(PhotonNetwork.NickName + " left the room");
    }

    public void JoinTeam(Team team)
    {
        localPlayer.team = team;
        
    }
    public void StartGame()
    {
        SceneManager.LoadScene(2);
    }
    public void Update()
    {
        foreach (Photon.Realtime.Player player in PhotonNetwork.PlayerList)
        {
            //players.Append(player.NickName + "\n");
        }
        //playersList.text = players.;
    }

    private void OnLevelWasLoaded(int level)
    {
        if (level >= 2)
        {
            string tmp;
            if (localPlayer.team == Team.Hunter) tmp = "hunterPlayer";
            else tmp = "ghostPlayer";
            GameObject player = PhotonNetwork.Instantiate(tmp, Vector3.zero, Quaternion.identity);
            player.GetComponent<Player>().PlayerData = localPlayer;
        }
    }
}

public class PlayerData
{
    public string Nickname
    { 
        get { return PhotonNetwork.NickName; }
        set { PhotonNetwork.NickName = value; }
    }
    public Team team;
}
public enum Team
{
    Ghost,
    Hunter
}
