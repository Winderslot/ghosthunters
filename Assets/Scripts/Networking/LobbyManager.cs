using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyManager : MonoBehaviourPunCallbacks
{

    void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        //PhotonNetwork.ConnectUsingSettings();
        Connect("10.3.17.6", 5055, "");

        PhotonNetwork.AuthValues = new AuthenticationValues();
        PhotonNetwork.AuthValues.UserId =
            System.DateTime.Now.ToString();



        PhotonNetwork.NickName = "player " + Random.Range(0, 123);
        Debug.Log(PhotonNetwork.NickName);

        
        
    }
    public void CreateRoom()
    {
        PhotonNetwork.CreateRoom(null, new Photon.Realtime.RoomOptions { MaxPlayers = 5});
    }
    public void JoinRoom()
    {
        PhotonNetwork.JoinRandomRoom();
    }
    public override void OnJoinedRoom()
    {
        Debug.Log("Joined");
        PhotonNetwork.LoadLevel("Lobby");
    }
    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected to master");
    }
    
    void Update()
    {
        
    }
    private void Connect(string ip, int port, string appID)
    {
        PhotonNetwork.ConnectToMaster(ip, port, appID);
        PhotonNetwork.GameVersion = "1";
    }

}
