﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEditor.UI;

public class Launcher : MonoBehaviourPunCallbacks
{
    [SerializeField] string ip;
    [SerializeField] string gameVersion = "1";
    [SerializeField] byte maxPlayers = 8;
    

    private void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        //PhotonNetwork.ConnectUsingSettings(); // cloud
        Connect(ip, 5055, ""); // self-hosted

        PhotonNetwork.AuthValues = new AuthenticationValues();
        PhotonNetwork.AuthValues.UserId = 
            System.DateTime.Now.ToString();
        print(PhotonNetwork.AuthValues.UserId);
    }
    public void JoinRoom()
    {
        if (PhotonNetwork.CountOfRooms == 0)
            PhotonNetwork.CreateRoom("game",
                new RoomOptions { MaxPlayers = maxPlayers });
        else
        {
            PhotonNetwork.JoinRoom("game");
        }
    }

    public override void OnJoinedRoom()
    {
        //PhotonNetwork.LoadLevel(1);
        
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        base.OnJoinRoomFailed(returnCode, message);
        print(returnCode.ToString());
        print(message);
    }


    private void Connect(string ip, int port, string appID)
    {
        PhotonNetwork.ConnectToMaster(ip, port, appID);
        PhotonNetwork.GameVersion = gameVersion;
    }
    private void Update()
    {
        /* if (Input.GetKeyDown(KeyCode.Return))
        {
            JoinRoom();
        }
        //print(PhotonNetwork.NetworkClientState); */
    }
}
