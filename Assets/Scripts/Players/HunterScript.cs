using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class HunterScript : Player
{
    GameObject enemy;
    public float enemyHP;
    Animator anim;
    public Text enemyHpText;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>(); 
    }
    public override void Update()
    {
        base.Update();
        if (this.GetComponent<Rigidbody>().velocity.x != 0)
        {
            anim.SetInteger("state", 1);
        }

        if (hit.collider)
        {
            if (hit.collider.gameObject.tag == "Ghost")
            {
                enemy = hit.collider.gameObject;
                enemyHP = enemy.GetComponent<GhostScript>().HP;
                if (enemyHP >= 0) { enemyHpText.text = "" + enemyHP; } else { enemyHpText.text = ""; }
            }
        }
        if (Input.GetMouseButtonDown(0))
        {
            Attack();
            if (hit.collider)
            {
                if (hit.collider.gameObject.tag == "Ghost")
                {
                    MakeDamage();
                }
            }
        }
    }
    void MakeDamage()
    {
        enemyHP = enemyHP - 3;
        enemy.GetComponent<GhostScript>().HP = enemyHP;
    }
    void Attack()
    {

    }
}
