using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GhostScript : Player
{
    float cooldown;    
    public float HP;
    public GameObject PlaceToTP;
    bool inObject = false;
    public bool untarget = false;
    public GameObject ghostMesh;

    public override void Update()
    {
        base.Update();

        if (Input.GetMouseButtonDown(0))
        {
            if (hit.collider)
            {
                if (hit.collider.gameObject.tag == "special")
                {
                    Settle();
                }
            }
        }
        if (inObject == true)
        {
            if (Input.GetMouseButton(1))
            {
                inObject = false;
                Out();
            }
        }
        if (HP <= 0)
        {
            Destroy(gameObject);
        }
    }
    public void Settle()
    {
        inObject = true;
        PlaceToTP.transform.position = gameObject.transform.position;
        gameObject.GetComponent<CapsuleCollider>().enabled = false;
        gameObject.GetComponent<Rigidbody>().isKinematic = true;
        gameObject.transform.position = hit.collider.gameObject.transform.position;
        ghostMesh.GetComponent<MeshRenderer>().enabled = false;
    }
    public void Out()
    {
        gameObject.transform.position = PlaceToTP.transform.position;
        gameObject.GetComponent<CapsuleCollider>().enabled = true;
        gameObject.GetComponent<Rigidbody>().isKinematic = false;
        ghostMesh.GetComponent<MeshRenderer>().enabled = true;
    }
}

