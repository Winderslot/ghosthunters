using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Player : MonoBehaviourPunCallbacks
{

    public PlayerData PlayerData;
    public RaycastHit hit;
    public Animator doorAnim;
    public float rayDistance = 4;
    public bool Open = false;
    void Start()
    {
        if (!photonView.IsMine && PhotonNetwork.IsConnected)
        {
            foreach (MeshRenderer renderer in gameObject.GetComponentsInChildren<MeshRenderer>())
            {
                renderer.enabled = false;
            }
        }
    }


    public virtual void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Physics.Raycast(ray, out hit, rayDistance);
        if (Input.GetMouseButtonDown(0))
        {
            if (hit.collider)
            {
                if (hit.collider.gameObject.tag == "Door")
                {
                    doorAnim = hit.collider.GetComponent<Animator>();
                    if (Open == false)
                    {
                        doorAnim.SetInteger("State", 1);
                        Open = true;
              
                    }
                    else
                    {
                        doorAnim.SetInteger("State", 0);
                        Open = false;
                      
                    }
                }
            }
        }
    }
}
