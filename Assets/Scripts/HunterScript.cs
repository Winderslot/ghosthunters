using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class HunterScript : MonoBehaviourPunCallbacks
{
    RaycastHit hit;
    public float rayDistance = 4;
    GameObject enemy;
    public float enemyHP;
    public Material enemyMaterial;
    public GameObject hunterMesh;
    Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>(); 
    }
    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Physics.Raycast(ray, out hit, rayDistance);
        if (hit.collider)
        {
            if (hit.collider.gameObject.tag == "Ghost")
            {
                enemy = hit.collider.gameObject;
            }
        }
        if (Input.GetMouseButton(0))
        {
            anim.SetInteger("state", 2);
            if (hit.collider)
            {
                if (hit.collider.gameObject == enemy)
                {
                    enemyHP = enemy.GetComponent<Raycast>().HP;
                    MakeDamage();
                }
            }
            
        }
        if(GetComponent<Rigidbody>().velocity.x != 0)
        {
            anim.SetInteger("state", 1);
        }
    }
    void MakeDamage()
    {
        enemyHP = enemyHP - 5 * Time.deltaTime;
        enemy.GetComponent<Raycast>().HP = enemyHP;

    }
}
